<?php
// $Id:$

/**
 * @file
 * keyclient menu items.
 *
 */

function uc_keyclient_complete($cart_id = 0) {
	$order_id = intval(substr(check_plain($_GET['codTrans']),0,6));
  watchdog('keyclient', 'Ricevuta una nuova notifica per l\'ordine !order_id.', array('!order_id' => $order_id));

  //$order = uc_order_load($_GET['codTrans']);
	$order = uc_order_load($order_id);

  if ($order === FALSE || uc_order_status_data($order->order_status, 'state') != 'in_checkout') {
    drupal_set_message("Si è verificato un errore durante il pagamento dell'ordine (ad es. ordine non trovato oppure già pagato). Contattateci per assicurarvi che l\'ordine sia stato inviato.");
    drupal_goto("cart");
		exit();
  }

  $mac = $_GET['mac'];
  // funzione per validare mac
	/*
  $valid = md5(variable_get('uc_keyclient_secret_word', 'esempiodicalcolomac') . $_GET['alias'] . $_GET['merchant_order_id'] . $_GET['total']);
  if (strtolower($key) != strtolower($valid)) {
    uc_order_comment_save($order->order_id, 0, 'Tentativo di completare ordine con un codice non verificato.', 'admin');
  }
*/

  if (strtolower($_GET['email']) !== strtolower($order->primary_email)) {
    uc_order_comment_save($order->order_id, 0, t('Il cliente ha usato un indirizzo e-mail diverso durante il pagamento: !email', array('!email' => check_plain($_GET['email']))), 'admin');
  }

	// TRANSACTION OK 
  if ($_GET['esito'] == 'OK' && is_numeric($_GET['importo'])) {
    $importo_ok = number_format($_GET['importo'] / 100, 2);
    $comment = t('Ordine #!order pagato su Key Client in data/ora !data - !orario con carta di credito !BRAND, codice autorizzazione !codAut da !nome !cognome - !email', array('!order' => $order->order_id, '!codAut' => check_plain($_GET['codAut']), '!data' => check_plain($_GET['data']), '!orario' => check_plain($_GET['orario']), '!nome' => check_plain($_GET['nome']), '!cognome' => check_plain($_GET['cognome']), '!email' => check_plain($_GET['email']), '!BRAND' => check_plain($_GET['$BRAND']) ));
    uc_payment_enter($order->order_id, 'keyclient', $importo_ok, 0, NULL, $comment);
	  // Empty that cart...
	  uc_cart_empty($cart_id);
	  // Save changes to order without it's completion (it will be on finalization step)
	  uc_order_save($order);
	
		// REDIRECTING TO FINALIZE
		/*
		$url = 'cart/keyclient/finalize/'. $order->order_id;
	  // Javascript redirect on the finalization page.
	  $output = '
			<script type="text/javascript">
				window.location = "'. url($url, array('absolute' => TRUE)) .'";
			</script>';
	  // Text link for users without Javascript enabled.
	  $output .= '<div id="finalize-link">'.l(t('Clic qui per completare l\'acquisto.'), $url, array('absolute' => TRUE)).'</div>';
	  // gateway sometines needs page larger than 255 characters to display.
	  while (strlen($output) < 255) {
	    $output .= '&nbsp;';
	  }
	  print $output;
		exit();
		*/
		$url = 'cart/keyclient/finalize/'. $order->order_id;
		drupal_goto($url);
  }
	// TRANSACTION KO
  else {
    drupal_set_message(t("Si è verificato un errore durante l'autorizzazione del pagamento, verificare i dati e riprovare"));
    uc_order_comment_save($order->order_id, 0, t("Si è verificato un errore durante l'autorizzazione del pagamento", array()), 'admin');
  	drupal_goto("cart");
		exit();
	}
  
}

function uc_keyclient_finalize() {
  $order = uc_order_load(arg(3));

	if (!($order === FALSE) && $order->order_status == "in_checkout") {
		// Add a comment to let sales team know this came in through the site.
	  uc_order_comment_save($order->order_id, 0, t('Ordine creato sul sito web.'), 'admin');
	  $output = uc_cart_complete_sale($order, variable_get('uc_new_customer_login', FALSE));
	  $page = variable_get('uc_cart_checkout_complete_page', '');
	  if (!empty($page)) {
	    drupal_goto($page);
	  }
 		return $output;
	} else {
		drupal_set_message("Ordine non trovato oppure già evaso");
		drupal_goto("");
	}

}

